﻿using UnityEngine;
using UnityEngine.AI;

public enum Statetype
{
    DEFAULT,
    //Default state
    WANDER,
    //wander while player is still
    COMMAND,
    //send command when player is in trouble
    FOLLOWING,
    //Follow when player is running
    AMMO_COMMAND
}
   ;
public class NPCController : MonoBehaviour
{

    #region public vars
    public int randomDistanceBeforeNPCStartFollowing = 40;
    #endregion

    #region private

    private Quaternion lookRotation;
    private Vector3 targetPos;
    private NavMeshAgent agent;
    private Statetype state;
    private float wanderTimer;
    private float timer;
    private float wanderRadius;
    private GameObject targetPlayer;
    private Transform localTransform;
    private float rotationSpeed = 10.0f;

    #endregion


    private void Start()
    {

        //Enabling Agent Here to prevent the teleporting
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = true;
        agent.speed = UnityEngine.Random.Range(10f, 13f);// the random speed of each NPC

        wanderTimer = 8;
        wanderRadius = 15.0f;
        timer = wanderTimer;

        targetPlayer = GameObject.FindWithTag("Player");
        localTransform = gameObject.transform;


    }


    // Update is called once per frame
    private void Update()
    {

        if (state != Statetype.AMMO_COMMAND)
        {
            if (canFollow())
            {
                state = Statetype.FOLLOWING;
            }
            else
            {
                state = Statetype.WANDER;
            }
        }

        controlMovement();
        RotateTowards(targetPos, rotationSpeed);

    }


    private bool canFollow()
    {
        float Distance = Vector3.Distance(targetPlayer.transform.position, localTransform.position);
        if (Distance > randomDistanceBeforeNPCStartFollowing)
            return true;
        else
            return false;
    }

    private void controlMovement()
    {
        timer += Time.deltaTime;
        if (state == Statetype.WANDER)
        {

            if (timer >= UnityEngine.Random.Range(11, 14))
            {
                Vector3 newPos = RandomNavSphere(targetPlayer.transform.position, wanderRadius, -1);
                agent.isStopped = false;
                targetPos = newPos;
                agent.SetDestination(newPos);
                timer = 0;
            }
        }
        else if (state == Statetype.FOLLOWING)
        {
            agent.isStopped = false;
            targetPos = targetPlayer.transform.position;
            agent.SetDestination(targetPlayer.transform.position);

        }
        else if (state == Statetype.AMMO_COMMAND)
        {
            //the trigger will work here if player assign command of ammo to NPC

        }
    }



    private void RotateTowards(Vector3 target, float speed)
    {
        if (target != null)
        {
            if (agent.desiredVelocity.magnitude > 0)
            {
                lookRotation = Quaternion.LookRotation(agent.desiredVelocity, Vector3.up);
            }
            else
            {
                lookRotation = Quaternion.LookRotation(target - localTransform.position);
                lookRotation.x = 0f;
                lookRotation.z = 0f;
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, speed * Time.deltaTime);
        }
    }


    private Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        NavMeshHit navHit;
        Vector3 randDirection = UnityEngine.Random.insideUnitSphere * dist;
        randDirection += origin;
        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }



}
