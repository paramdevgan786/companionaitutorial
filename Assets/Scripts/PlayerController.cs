﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{

    public float moveSpeed = 10.0f;
    // Character movement speed.
    public int rotationSpeed = 2;
    // How quick the character rotate to target location.
    public float gravity = 20.0f;
    // Gravity for the character.
    public float jumpSpeed = 8.5f;
    // The Jump speed

    #region private var

    private Transform transfromReference;
    private Vector3 moveDirection = Vector3.zero;
    private float xMovement, zMovement;
    private CharacterController controller;
    private float overAllSpeed;
    #endregion

    void Start()
    {

        transfromReference = transform;
        // Get the player character controller.
        controller = transfromReference.GetComponent<CharacterController>();

    }

    void Update()
    {
        xMovement = Input.GetAxis("Horizontal");// The horizontal movement.
        zMovement = Input.GetAxis("Vertical");// The vertical movement.

        if (IsGrounded())
        {
            transform.Rotate(0, xMovement * moveSpeed, 0);
            moveDirection = transform.TransformDirection(Vector3.forward);

        }
        else
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        overAllSpeed = rotationSpeed * zMovement;
        controller.SimpleMove(moveDirection * overAllSpeed);
    }

    bool IsGrounded()
    {
        return controller.isGrounded;
    }


}